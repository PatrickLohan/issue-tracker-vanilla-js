document.getElementById("issueInputForm").addEventListener("submit", saveIssue);

function saveIssue(e) {
  let issueDesc = document.getElementById("issueDescInput").value;
  let issueSeverity = document.getElementById("issueSeverityInput").value;
  let issueAssignedTo = document.getElementById("issueAssignedToInput").value;
  let issueId = uuidv4();
  let issueStatus = "Open";

  let issue = {
    id: issueId,
    description: issueDesc,
    severity: issueSeverity,
    assignedTo: issueAssignedTo,
    status: issueStatus,
  };

  if (localStorage.getItem("issues") == null) {
    let issues = [];
    issues.push(issue);
    localStorage.setItem("issues", JSON.stringify(issues));
  } else {
    let issues = JSON.parse(localStorage.getItem("issues"));
    issues.push(issue);
    localStorage.setItem("issues", JSON.stringify(issues));
  }

  // Tidy up
  document.getElementById("issueInputForm").reset();
  fetchIssues();
  e.preventDefault();
}

function fetchIssues() {
  // TODO change this to use a backend db

  // get issue from browser local storage
  let issues = JSON.parse(localStorage.getItem("issues"));

  // retrieve reference to issuesList element
  let issuesList = document.getElementById("issuesList");

  // initialise empty
  issuesList.innerHTML = "";

  // declare the variables for the for loop so they don't get hoisted and recalculated for each loop
  let issuesLength = issues.length,
    i;
  // for each issue generate html output and add to issuesList
  for (i = 0; i < issuesLength; i++) {
    let id = issues[i].id;
    let desc = issues[i].description;
    let severity = issues[i].severity;
    let assignedTo = issues[i].assignedTo;
    let status = issues[i].status;

    issuesList.innerHTML +=
      '<div class="issue"' +
      "<h6>Issue ID: " +
      id +
      "</h6>" +
      '<p><span class="label label-info">' +
      status +
      "</span></p>" +
      "<h3>" +
      desc +
      "</h3>" +
      '<p><span class="fas fa-clock"></span>' +
      " " +
      severity +
      "</p>" +
      '<p><span class="fas fa-user"></span>' +
      " " +
      assignedTo +
      "</p>" +
      '<a href="#" onclick="setStatusClose(\'' +
      id +
      '\')" class="button button-close">Close</a>' +
      '<a href="#" onclick="deleteIssue(\'' +
      id +
      '\')" class="button button-delete">Delete</a>' +
      "</div>";
  }
}

function uuidv4() {
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, (c) =>
    (
      c ^
      (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
    ).toString(16)
  );
}
